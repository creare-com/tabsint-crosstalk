/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        var receivedMessageEl = document.getElementById('receivedMessage');
        var receivedDataEl = document.getElementById('receivedData');
        var returnMessageEl = document.getElementById('returnMessage');
        var returnDataEl = document.getElementById('returnData');

        receivedMessageEl.innerHTML = 'Waiting';
        receivedDataEl.innerHTML = 'Waiting';
        returnMessageEl.value = 'Default Return Message';
        returnDataEl.value = 'Default Return Data';

        document.getElementById("sendResponse").addEventListener("click", function(){
          var returnMessage = returnMessageEl.value;
          var returnData = returnDataEl.value;
          // assemble response
          var dataOut = {
            message: returnMessage,
            data: returnData
          };
          console.log('Sending');console.log(dataOut);

          // Convert response object into string using JSON.stringify
          dataOutString = JSON.stringify(dataOut);
          Receiver.sendExternalData(function(){console.log('Success sending interapp message')},function(e){console.log(e)}, 'com.creare.skhr.tabsint', dataOutString);
        });

        function respondToData(d){

          var dataIn

          // Ensure data is defined and correct type
          if (d.type && d.type === 'text/plain' && d.data) {
            try {
              var dataIn = JSON.parse(d.data);
            } catch (e) {
              console.log('ERROR: Could not parse incoming data object.  Error: ' + JSON.stringify(e) + '.  The incoming data object should be a JSON.stringified object.  Received: ' + d);
            }
          }
          console.log('Received interapp message.');console.log(dataIn);

          receivedMessageEl.innerHTML = dataIn.message || 'No Message';
          receivedDataEl.innerHTML = JSON.stringify(dataIn.data) || 'No Data';

          // // Automatically generate and send a response after 3 seconds
          // // assemble response
          // var dataOut = {
          //   message: 'Returning ' + (dataIn.message || 'Default Message'),
          //   data: dataIn.data || 'Default Data'
          // };
          // console.log('Returning');console.log(dataOut);
          //
          // // Convert response object into string using JSON.stringify
          // dataOutString = JSON.stringify(dataOut);
          // setTimeout(function(){
          //   Receiver.sendExternalData(function(){console.log('Success sending interapp message')},function(e){console.log(e)}, 'com.creare.skhr.tabsint', dataOutString);
          // }, 3000);
        }

        // Get first message, in case this app wasn't yet loaded.  The intent handler will not get fired for data already received.
        // Receiver.getExternalDataIntent(  function(d){console.log('Success getting initial message.');respondToData(d);},  function(e){console.log('Error: ' + JSON.stringify(e));}  );
        Receiver.setExternalDataIntentHandler(respondToData, function(e){console.log('Error: ' + JSON.stringify(e));}  );

        // // Automatically send a response 3 seconds after starting
        // setTimeout(function(){
        //   var dataOut = {message: 'Testing External Data', data: '42'};
        //   console.log('testing send with message: ' + JSON.stringify(dataOut));
        //   Receiver.sendExternalData(function(){console.log('success')},function(e){console.log(e)},'com.creare.skhr.tabsint',dataOut);
        // }, 3000);

        navigator.notification.alert('This app can communicate with TabSINT!');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }

};

app.initialize();
